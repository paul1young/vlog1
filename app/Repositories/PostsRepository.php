<?php namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Posts;

class PostsRepository{

	public function index()
	{
		return Posts::with('user')->orderBy('id')->get();
	}

}