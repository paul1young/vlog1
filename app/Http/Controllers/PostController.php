<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostsRepository;
use Illuminate\Support\Facades\DB;
use App\Posts;
use App\User;
use App\Rules\SwearWords;
use App\Mail\postsEmail;
use App\Models\Leaderboard;
use Mail;
use App\Repositories\leaderboardRepository;
		


class PostController extends Controller
{
	private $postsRepository;

	public function __construct(PostsRepository $postsRepository){
		$this->postsRepository = $postsRepository;

	} 

 	public function index() 
 	{
 		return view ('index');

	}

	public function show() 
 	{

 		// 1 create edit link in form
 		// 2 create edit method in class- which pulls id from request input
 		// 3 create form which is populated by posts::find() 
 		// 4 create method which updates record using post::find() and save()
 		// 5 redirect back to your comments


 		// 1 create delete link in form
 		// 2 create delete method in class- which pulls id from request input
 		// 3 create method which deletes record using post::find() and delete() 
 		// 4 redirect back to your comments

 		

 		$posts = $this->postsRepository->index();

 		return view ('about', [
 			'posts' => $posts
 			]
 		);

// $swear = array();
     
   //  $swear[] = '/[^a-zA-Z]+?(fucking)[^a-zA-Z]??/i';
    // $swear[] = '/(fucker)[^a-zA-Z]??/i';
   //  $swear[] = '/[^a-zA-Z]+?(fuckin)[^a-zA-Z]??/i';
   //  $swear[] = '/^(fuck)[^a-zA-Z]??/i' ;
   //  $swear[] = '/[^a-zA-Z]+?(fucks)[^a-zA-Z]??/i';
   //  $swear[] = '/^(fuk)[^a-zA-Z]??/i';
      

      
   //  $string = return preg_replace("/fucking|fucker|fuck/i", '!@#$%', $string);  
     // stripos
     
    // return $string; 





	}

	public function create() 
 	{
 		return view ('create');
	}

	public function store() 
	{

	  $authId = Auth()->id();
	  	
	  $post = new Posts;

	  $this->validate(Request(),
 			[
 			'title'=>'min:3|max:160|required',
 			'body'=>['min:3|max:160|required',new SwearWords],
 			'phone'=>'min:11|max:11|required'
 			]
 		);

	  $post->title = Request()->input('title');
	  $post->body = Request()->input('body');
	  $post->phone = Request()->input('phone');
	  $post->user_id = Auth()->id();

	  if ($post->save()) 
	  {
	  	$user = User::find($authId);

	  	sendNotificationEmail($user);
      }

      return redirect('/posts/show');
	 }

	


	 /* public function sendNotificationEmail(User $user)
	  {


	  	Mail::send('posts.email.postEmail', ['user'=> $user->name], function ($m) use ($user)
    	{
    		$m->from('paul@pearlai.com', 'Joe Bloggs Apps');
    		$m->to('paul@pearlai.com', 'paul')->subject('posts!');
    	});

	  }
	  */

	public function delete()
	{
		$post = Posts::find(Request()->input('id'));

		$post->delete();

		return redirect('/posts/show');
	}


 	public function getEdit()
 	{
 		
 		$post = Posts::find(Request()->input('id')) ;

		return view ('posts.edit',['post'=>$post]);
 			
 	}

 	public function postEdit()
 	{
 		$this->validate(Request(),
 			[
 			'title'=>'min:3|max:160|required',
 			'body'=>'min:3','max:160','required',
 			]
 		);

 		$post = Posts::find(Request()->input('id')) ;

 		$post->title = Request()->input('title');
	  	$post->body = Request()->input('body');

  	 	$post->save();

	  	return redirect('/posts/show');
	} 

	public function funpage()
	{

        return view ('/funpage');

	}

/*public function save  score()
	{
		$score = Request()->input('score');
        return view ('/score', ['score' => $score]);
    }
*/
	
	public function getScore()
 	{
 		
	  	return View('score',['score'=>Request()->input('score')]);
	} 


	public function postScore()
 	{
 		
 		$funpage = new leaderboard ;

 		$funpage->name = Request()->input('name');
 		$funpage->score = Request()->input('score');
	  	
  	 	$funpage->save();

	  	return redirect('/posts/leaderboard');

	} 


	

		

	public function leaderboard()
	{
   		 $leaderboardRecords = DB::table('leaderboard')->get();

   		 $scoreArray = [];
   		 $nameArray = [];
   		 $idArray = [];

   		 // foreach ($leaderboardRecords as $record)

   		 //  {
   		 // 	$scoreArray[] = $record->score;
   		 // 	$namearray[] =$record->name;
   		 // 	$idarray[] = $record->id;
   		 // }
   		 
   	// return view ('/leaderboard',[ 'score' => $leaderboard->score, 'name'=> $leaderboard->name, 'id'=> $leaderboard->id]);
		 
   	return view ('/leaderboard',['leaderboard'=>$leaderboardRecords]);

			

	}

}
