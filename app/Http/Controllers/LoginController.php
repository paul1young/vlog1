<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostsRepository;
use App\Repositories\LoginRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Posts;
use App\User;


class LoginController extends Controller
{
	private $loginRepository;

	public function __construct(loginRepository $loginRepository)
	{
		$this->loginRepository = $loginRepository;
	} 

	public function getRegister()
	{  
		return view('register');
	}

 	public function postRegister()
	{


		$newuser = new User;
		$newuser->name = Request()->input('name');
		$newuser->email = Request()->input('email');
		$newuser->password =  Hash::make(Request()->input('password'));

		$newuser->save(); 

		Auth()->login($newuser);

		return redirect ('/');

	 }
		// redirect and show logged in user 


	public function getLogin()
	{
		 return view ('login');
	}

 	
	public function postLogin() 
 	{

 		$this->validate(Request(),
			[
 			'email'=>'min:5|required',
 			'password'=>'min:3|required'
 			]
 		);


  		if (Auth::attempt(['email' => Request()->input('email'), 'password' => Request()->input('password')], Request()->input('remember'))) 
		{
		    return Redirect('/');
		}
		
	}

	public function Logout(){
		Auth()->logout();

		return redirect ('/');
	}

}
