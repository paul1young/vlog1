<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leaderboard extends Model
{

	protected $table = 'leaderboard';

	protected $primaryKey = 'id';

	// protected $timestamps = false;

	protected $fillable = [
			'name',
			'score'
	];

  
}