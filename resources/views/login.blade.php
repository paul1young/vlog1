@extends('layout')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
      </div>
    @endif

  <form class="px-4 py-3"  method="post"  action="{{url('/login')}}" >

  	    {{ csrf_field() }}

    <div class="form-group">
      <label for="exampleDropdownFormEmail1">Email address</label>
      <input type="email" class="form-control" name="email" id="exampleDropdownFormEmail1" placeholder="email@example.com">
    </div>
    <div class="form-group">
      <label for="exampleDropdownFormPassword1">Password</label>
      <input type="password" class="form-control" name="password" id="exampleDropdownFormPassword1" placeholder="Password">
    </div>
    <div class="form-check">
      <label class="form-check-label">
        <input type="checkbox" name="remember" value="1" class="form-check-input">
        Remember me
      </label>
    </div>
    <button type="submit">Sign in</button>
  </form>


@endsection