 @extends ('layout')
@section('content')

@foreach($posts as $post)
 	

 	

<div class="card w-75" style="margin-top:30px">
  <div class="card-block">
    <h3 class="card-title">{{$post->title}}</h3>
    <p class="card-text">{{$post->body}}</p>
   
   
   <small class="text-muted">{{$post->created_at->diffForHumans()}}</small>
 
 		<a class="nav-link" href="{{url('/posts/edit?id='.$post->id)}}">edit</a>
   		<a class="nav-link" href="{{url('/posts/delete?id='.$post->id)}}">delete</a>
   		@if (!empty($post->user->name))
   			<p>{{ $post->user->name }}</p>
   		@endif


 </div>
</div>

@endforeach

@endsection
