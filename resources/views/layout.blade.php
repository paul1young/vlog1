
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Blog Template for Bootstrap</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
    
    <style>
    @yield('css')
    </style>

  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="nav blog-nav ">
          <a class="nav-link active" href="{{url('')}}">Home</a>
          <a class="nav-link" href="{{url('/posts/show')}}">all comments</a>
          <a class="nav-link" href="{{url('')}}">Add Comment</a>
           <a class="nav-link" href="{{url('/register')}}">new user!</a>
           <a class="nav-link" href="{{url('/posts/funpage')}}">funpage!!!</a>
            
          @if(Auth()->check()) <a class="nav-link" <a href="{{url('/logout') }}">log out</a>  @else <a class="nav-link" href="{{url('/login')}}">login</a> @endif
 


        </nav>
        <div class="float-right">
          @if(Auth()->check())
            {{Auth()->id()}}
            {{Auth()->user()->email}}
          @endif
        </div>
      
      </div>
    </div>

    <div class="blog-header">
      <div class="container">
        <h1 class="blog-title">The Bootstrap Blog</h1>
        <p class="lead blog-description">pls add a random comment.</p>
         
      </div>
    </div>

<div class="container">
    @yield('content')
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/core.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>   
         