@extends ('layout')
@section('content')
<div class="col-sm-8 blog-main">

      <div class="blog-header">
      <div class="container">
        <h1 class="blog-title">Edit a comment pls</h1>
       
      </div>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
      </div>
    @endif


      <form method = "post" action= "/posts/edit">

      <input type="hidden" name="id" value="{{$post->id}}">

      {{csrf_field()}}

      <div class="form-group">
        <label for="title">Title</label>
        <input type="title" class="form-control" id="title"  name="title" value="{{$post->title}}">
        <small id="" class=""></small>
      </div>
      
     <div class="form-group @if($errors->has('body')) has-danger @endif">
        <label class="control-label">body</label>
        <div class="controls">
            <input type="title" class="form-control" id="title"  name="body" value="{{$post->body}}">

            @if($errors->first('body'))
                <div class="form-control-feedback">{{$errors->first('body')}}</div>
            @endif
        </div>
    </div>


    
      <button type="submit" class="btn btn-primary">edit Comment </button>
    </form>
    
  </div>
<script>



  @endsection