<?php
//Route::post('your_form_url', ['as' => 'form_url', 'uses' => 'your_controller_name@save_data']);
route::get('posts/score ','PostController@getScore');
route::post('posts/score ','PostController@postScore');


route::get('posts/funpage ','PostController@funpage');
route::get('posts/leaderboard ','PostController@leaderboard');




route::get('/posts/show','PostController@show');




Route::group(['middleware' => 'auth'],function(){

	route::get('/','PostController@index');

	route::get('/posts/create','PostController@create');

	Route::post('/post','PostController@store');
	route::get('/posts/edit','PostController@getEdit');

	route::post('/posts/edit','PostController@postEdit');

	route::get('/posts/delete','PostController@delete');

});


route::get('/login','loginController@getLogin')->name('login');
route::post('/login','loginController@postlogin');


route::get('/logout','loginController@logout');



route::get('/register','LoginController@getRegister');
route::post('/register','LoginController@postRegister');







// route::get('posts','postsController@users');
